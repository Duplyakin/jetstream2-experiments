#!/bin/bash

# Run and save raw results
start=`date +%s.%N`
node tagcloud.js
run_status=$?
end=`date +%s.%N`

runtime=$( echo "$end - $start" | bc -l )

benchmark_result_dest=results/results.txt
echo $runtime >> $benchmark_result_dest

exit $run_status
