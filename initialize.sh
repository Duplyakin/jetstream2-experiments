#!/bin/bash

# Move into the directory with the code from this repo (the tool will clone it; no need to clone it using this script) 
# Currently, it is important the name of the directory here matches the name of the repo
cd jetstream2-experiments

# Set up directory for results, delete if already exists
rm -rf results
mkdir results

# Install Packages
echo -n "Installing Packages "
date
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update -y
sudo apt-get install -y bc
curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs=16.11.1-deb-1nodesource1
node -—version

curl https://browserbench.org/JetStream/SeaMonster/gaussian-blur.js -O
# Modify the script to make it run longer and actually execute one iteration (otherwise, the original script doesn't include execution part)
sed -i '/this.width = 800;/c\this.width = 8000;'  gaussian-blur.js
sed -i '/this.height = 450;/c\this.height = 4500;'  gaussian-blur.js
echo -e "let b = new Benchmark();\nfor (let iter = 0; iter < 1; iter++) {b.runIteration(); }" >>  gaussian-blur.js

curl https://browserbench.org/JetStream/SunSpider/tagcloud.js -O
# Modify the script to make it run longer 
sed -i '/for (let i = 0; i < 4; ++i)/c\for (let i = 0; i < 200; ++i)' tagcloud.js 
# Modify the script to make it runable
echo -e "let b = new Benchmark();\nb.runIteration();" >> tagcloud.js

curl https://browserbench.org/JetStream/Octane/box2d.js -O
# Modify the script to make it runable
echo -e "let b = new Benchmark();\nfor (var iter = 0; iter < 100; iter++) { b.runIteration(); }" >> box2d.js


# Make copies of experiment scripts and add installation of specific versions of node 
# Create different permutations and populate extp_config.py with created experiments
echo "" > exp_config.py 
for script in gaussian-blur.sh tagcloud.sh box2d.sh
do
    for ver in 16.11.1 15.14.0 14.18.1 
    do
        major_ver=`echo $ver | cut -d "." -f 1`
        echo "#!/bin/bash" > tmp
        echo "curl -sL https://deb.nodesource.com/setup_$major_ver.x | sudo -E bash -" >> tmp
        echo "sudo apt-get install -y --allow-downgrades nodejs=$ver-deb-1nodesource1" >> tmp
        tail -n+2 $script >> tmp
        mv tmp "node-$ver-$script"
        chmod +x "node-$ver-$script"

        echo "print(\"./node-$ver-$script\")" >> exp_config.py
    done
done

printf "Gaussian blur tests are ready to run now."
printf "tagcloud tests are ready to run now."
printf "box2d tests are ready to run now."
